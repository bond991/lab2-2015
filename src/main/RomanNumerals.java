package main;

//Nome coppie
//Silvia Pedersoli
//Giorgio Franzoni

public class RomanNumerals {

    public String arabicToRoman(int i) {
	String [] simboli=new String[]{"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
	int [] numeri= new int [] {1000,900,500,400,100,90,50,40,10,9,5,4,1};
	String risultato = "";
	
	
	risultato = scelta(i);
	
	int j = i % 10;

	if (j <= 9)
	    risultato = risultato + nove(j, "I");

	return risultato;

    }

    public String tre(int i, String simbolo) {
	String ris = "";
	for (int j = 1; j <= i; j++)
	    ris = ris + simbolo;
	return ris;
    }

    public String nove(int i, String simbolo) {
	String risnove = "";
	StringBuilder quattro=new StringBuilder("");
	StringBuilder cinque=new StringBuilder("");
	StringBuilder nove=new StringBuilder("");
	
	controllasimbolo(simbolo, quattro, cinque, nove);
	
	if (i - 5 <= 0) {
	    if (i < 4) {
		risnove = tre(i, simbolo);
	    } else if (i == 4)
		risnove = quattro.toString();
	    else if (i == 5)
		risnove = cinque.toString();
	} else if (i >= 6 && i < 9)
	    risnove = cinque + tre(i - 5, simbolo);
	else if (i == 9)
	    risnove = nove.toString();
	return risnove;

    }


    public String scelta(int i) {
	String j = "";
	int x = i / 10;
	if (x <= 9)
	    j = j + nove(x, "X");
	else if(x==10)
	    j="C";
	else if(x==40)
	    j="CD";
	return j;
    }
    
    public void controllasimbolo(String simbolo1, StringBuilder quat, StringBuilder cinq, StringBuilder nov)
    {
	if(simbolo1.equals("I")){
	    quat.append("IV");
	    cinq.append("V");
	    nov.append("IX");
	}
	else if(simbolo1.equals("X")) {
	    quat.append("XL");
	    cinq.append("L");
	    nov.append("XC"); 
	    
	    
	}
	    
	    
	
    }
}
