package test;

import static org.junit.Assert.*;
import main.RomanNumerals;

import org.junit.Before;
import org.junit.Test;

public class RomanNumeralsTest {

	private RomanNumerals romanNumerals = new RomanNumerals();

	@Before
	public void setUp(){
		
	}
	
	@Test
	public void trentatre() 
	{
	    assertEquals("33", "XXXIII", romanNumerals.arabicToRoman(33));
	}
	
	@Test
	public void one() {
		assertEquals("1", "I", romanNumerals.arabicToRoman(1));
	}
	
	@Test
	public void oneTwo() {
	    assertEquals("1", "I", romanNumerals.arabicToRoman(1));
	    assertEquals("2", "II", romanNumerals.arabicToRoman(2));
    }
	
	@Test
	public void oneTwoThree() {
		assertEquals("1", "I", romanNumerals.arabicToRoman(1));
		assertEquals("2", "II", romanNumerals.arabicToRoman(2));
		assertEquals("3", "III", romanNumerals.arabicToRoman(3));
	}
	
	@Test
	public void four() {
		assertEquals("4", "IV", romanNumerals.arabicToRoman(4));
	}
	
	@Test
	public void five() {
		assertEquals("5", "V", romanNumerals.arabicToRoman(5));
	}
	
	@Test
	public void six() {
		assertEquals("6", "VI", romanNumerals.arabicToRoman(6));
	}
	
	@Test
	public void seven() {
		assertEquals("7", "VII", romanNumerals.arabicToRoman(7));
	}
	
	@Test
	public void eight() {
		assertEquals("8", "VIII", romanNumerals.arabicToRoman(8));
	}
	
	@Test
	public void nineIsXPrefixedByI() {
		assertEquals("9", "IX", romanNumerals.arabicToRoman(9));
	}
	
	@Test
	public void ten() {
		assertEquals("10", "X", romanNumerals.arabicToRoman(10));
	}
	
	@Test
	public void eleven() {
		assertEquals("11", "XI", romanNumerals.arabicToRoman(11));
	}
	
	@Test
	public void twelve() {
		assertEquals("12", "XII", romanNumerals.arabicToRoman(12));
	}
	
	@Test
	public void thirteen() {
		assertEquals("13", "XIII", romanNumerals.arabicToRoman(13));
	}
	
	@Test
	public void fourteen() {
		assertEquals("14", "XIV", romanNumerals.arabicToRoman(14));
	}
	
	@Test
	public void fifteen() {
		assertEquals("15", "XV", romanNumerals.arabicToRoman(15));
	}
	
	@Test
	public void fourty() {
		assertEquals("40", "XL", romanNumerals.arabicToRoman(40));
	}
	
	@Test
	public void fifty() {
		assertEquals("50", "L", romanNumerals.arabicToRoman(50));
	}
	
	@Test
	public void ninety() {
		assertEquals("90", "XC", romanNumerals.arabicToRoman(90));
	}
	
	@Test
	public void oneHundred() {
		assertEquals("100", "C", romanNumerals.arabicToRoman(100));
	}
	
	@Test
	public void fourHundred() {
		assertEquals("400", "CD", romanNumerals.arabicToRoman(400));
	}

}
